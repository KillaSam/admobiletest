module.exports = {
  mode: 'jit',
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: {
        commonButton: 'linear-gradient(95deg, #319795 0%, #3182CE 100%)',
        topHeaderLine: 'linear-gradient(91deg, #319795 0%, #3182CE 100%)',
        hoverButton: 'linear-gradient(95deg, #2C7A7B 0%, #2B6CB0 100%)',
        titleLinear: 'linear-gradient(141deg, #EBF4FF 0%, #E6FFFA 100%)',
        centerBlockLinear: 'linear-gradient(134deg, #E6FFFA 0%, #EBF4FF 100%)'
      }
    },
    colors: {
      white: '#FFFFFF',
      black: {
        DEFAULT: '#000000',
        800: '#2D3748',
        600: '#4A5568'
      },
      gray: {
        DEFAULT: '#718096',
        100: '#F7FAFC',
        200: '#E6FFFA',
        300: '#38B2AC',
        400: '#CBD5E0',
        500: '#81E6D9',
        600: '#00000029',
        700: '#00000033',
        800: '#319795',
        900: '#718096'
      },
      brown: '#707070',
      blue: {
        DEFAULT: '#3182CE',
        300: '#EBF4FF'
      }
    },
    boxShadow: {
      header: '0px 3px 6px #00000029',
      footer: '0px -1px 3px #00000033'
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
