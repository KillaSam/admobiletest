export interface ITab {
  name: string,
  title: string,
  items: IItem[]
}

export interface IItem {
  text: string
  img: string
}